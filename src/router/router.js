import Vue from 'vue'
import Router from 'vue-router'
import IndexPage from '../components/pages/IndexPage'
import UserAuthorization from '../components/views/UserAuth'
import AuthInformation from '../components/pages/AuthInformation'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      component: {
        template: '<router-view></router-view>'
      },
      children: [
        {
          path: '/',
          component: IndexPage,
          children: [{
            path: '',
            name: 'IndexPage',
            components: {
              mainView: IndexPage,
              success: UserAuthorization
            }
          },
          {
            path: '/auth',
            name: 'UserAuthorization',
            components: {
              success: UserAuthorization
            }
          },
          {
            path: '/authInfo',
            name: 'AuthInformation',
            components: {
              rejected: AuthInformation
            }
          }]
        }
      ]
    }
  ],
  linkActiveClass: 'active'
})

export default router

router.addRoutes([{
  path: '/',
  component: IndexPage
}])
