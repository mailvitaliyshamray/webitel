
export default new function () {
  const services = {
    webitel: {
      login: 'https://cloud-ua1.webitel.com/engine/login',
      accounts: 'https://cloud-ua1.webitel.com/engine/api/v2/accounts',
      logout: 'https://cloud-ua1.webitel.com/engine/logout'
    }
  }
  this.linkTo = function () {
    return services.webitel
  }
}()
