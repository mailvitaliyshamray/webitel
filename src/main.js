import Vue from 'vue'
import App from './App.vue'
import Axios from 'axios'
import VueAxios from 'vue-axios'
import iView from 'iview'
import locale from 'iview/dist/locale/en-US'
import router from './router/router'
import Vuelidate from 'vuelidate'
import VueClipboard from 'vue-clipboard2'
import 'iview/dist/styles/iview.css'
import './assets/customize/index.less'
import VueCookies from 'vue-cookies-ts'

Vue.use(VueAxios, Axios)
Vue.use(iView, { locale })
Vue.use(Vuelidate)
Vue.use(VueClipboard)
Vue.use(VueCookies)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
