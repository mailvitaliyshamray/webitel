import WebClient from '../../../src/services/WebClient'
import Axios from 'axios'

jest.mock('axios', () => {
  var requestMock = function (url, data) {
    if (url === '/paramErrorUrl') {
      return Promise.resolve({
        data: {
          Message: 'paramError',
          status: 403
        }
      })
    }
    else {
      return Promise.resolve({
        data: {
          disclaimer: {},
          Data: 'testData' + url + data
        }
      })
    }
  }
  return {
    get: jest.fn((url) => requestMock(url, '')),
    put: jest.fn((url, data) => requestMock(url, data)),
    post: jest.fn((url, data) => requestMock(url, data))

  }
})

describe('WebClient.js', () => {

  beforeEach(() => {
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('should calback get param error', async () => {
    // Setup
    var testResult = null
    var testStatus = null
    const testParamErrorUrl = '/paramErrorUrl'

    // Act
    await WebClient.post(testParamErrorUrl, message => {
      testResult = message
    },
      (status, error) => {
        testStatus = status
      })

    // Assert
    expect(testResult).toBeNull()
    expect(testStatus.status).toEqual(403)
    expect(testStatus.Message).toEqual('paramError')
  })

  it('should get', async () => {
    // Setup
    const testUrl = 'latest.json'
    const testData = 'testData'
    const testUrlFull = 'testDatalatest.json'
    var testResult = null

    // Act
    await WebClient.get(testUrl, message => {
      testResult = message.Data
    })

    // Assert  testData/api/url
    expect(testResult).toEqual(testUrlFull)
  })

})
