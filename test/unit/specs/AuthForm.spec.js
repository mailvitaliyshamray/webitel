import Vue from 'vue'
import AuthForm from '../../../src/components/views/form/AuthForm'
import iView from 'iview'
import locale from 'iview/dist/locale/en-US'
import Vuelidate from 'vuelidate'
import VueClipboard from 'vue-clipboard2'

describe('UserAutorisation.vue', () => {
  let Constructor
  const Comp = AuthForm

  function create () {
    return new Constructor().$mount()
  }

  beforeEach(() => {
    Constructor = Vue.extend(Comp)
    Vue.use(iView, { locale })
    Vue.use(Vuelidate)
    Vue.use(VueClipboard)
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('Should $_sendToAutorization calls $emit with params', () => {
    // Setup
    const vm = create()
    const param = { 'username': 'username', 'password': 'password' }
    vm.$emit = jest.fn()
    vm.username = 'username'
    vm.password = 'password'

    // Action
    vm.$_sendToAutorization()

    // Assert
    expect(vm.$emit).toHaveBeenCalledTimes(1)
    expect(vm.$emit).toHaveBeenCalledWith('onUserAuthorization', param)
  })

  it('Should $_isVerify return false', () => {
    // Setup
    const vm = create()
    const text = '1234567'

    // Action
    const resultReceived = vm.$_isVerify(text)

    // Assert
    expect(resultReceived).toBeFalsy()
  })

  it('Should $_isVerify return true', () => {
    // Setup
    const vm = create()
    const text = '12345678'

    // Action
    const resultReceived = vm.$_isVerify(text)

    // Assert
    expect(resultReceived).toBeTruthy()
  })

  it('Should $_isVerify return true', () => {
    // Setup
    const vm = create()
    const text = '12345678'

    // Action
    const resultReceived = vm.$_isVerify(text)

    // Assert
    expect(resultReceived).toBeTruthy()
  })

  it('Should $_isDataToCopy calls onError', () => {
    // Setup
    const vm = create()
    vm.onError = jest.fn()
    vm.copyUserName = ''

    // Action
    vm.$_isDataToCopy()

    // Assert
    expect(vm.onError).toHaveBeenCalledTimes(1)
    expect(vm.onError).toHaveBeenCalled()
  })
})
