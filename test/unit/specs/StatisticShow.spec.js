import Vue from 'vue'
import StatisticShow from '../../../src/components/views/StatisticShow'

describe('StatisticShow.vue', () => {
  let Constructor
  const Comp = StatisticShow

  function create () {
    return new Constructor().$mount()
  }

  beforeEach(() => {
    Constructor = Vue.extend(Comp)
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('Should $_isDataEmpty return text', () => {
    // Setup
    const vm = create()
    const resultExpected = 'No informations'

    // Action
    const resultReceived = vm.$_isDataEmpty()

    // Assert
    expect(resultReceived).toEqual(resultExpected)
  })

  it('Should $_extractStatisticsData calls $_createListToDisplaying', () => {
    // Setup
    const vm = create()
    vm.$_createListToDisplaying = jest.fn()

    // Action
    vm.$_createListToDisplaying()

    // Assert
    expect(vm.$_createListToDisplaying).toHaveBeenCalledTimes(1)
    expect(vm.$_createListToDisplaying).toHaveBeenCalled()
  })

  it('Should $_createListToDisplaying return data list', () => {
    // Setup
    const vm = create()
    const item = {
      id: 1,
      name: 'name',
      status: 'status',
      role: 'user',
      domain: 'domain@Uint8Array.con',
      online: false
    }
    const resultExpected = [{
      id: 1,
      name: 'name',
      status: 'status',
      role: 'user',
      domain: 'domain@Uint8Array.con',
      online: 'offline',
      onlineStyle: 'red'
    }]
    vm.statisticsData = { 1: item }

    // Action
    const resultReceived = vm.$_createListToDisplaying()

    // Assert
    expect(resultReceived).toEqual(resultExpected)
  })

  it('Should $_getUserInfo return data array and style online text should be red', () => {
    // Setup
    const vm = create()
    const item = {
      id: 1,
      name: 'name',
      status: 'status',
      role: 'user',
      domain: 'domain@Uint8Array.con',
      online: false
    }
    const resultExpected = {
      id: 1,
      name: 'name',
      status: 'status',
      role: 'user',
      domain: 'domain@Uint8Array.con',
      online: 'offline',
      onlineStyle: 'red'
    }

    // Action
    const resultReceived = vm.$_getUserInfo(item)

    // Assert
    expect(resultReceived).toEqual(resultExpected)
  })
})
